#!/usr/bin/perl

use strict;

use IO::Socket::SSL;

srand( time() );

my $firstPhrase = [
    "artless",
    "bawdy",
    "beslubbering",
    "bootless",
    "churlish",
    "clouted",
    "cockered",
    "craven",
    "currish",
    "dankish",
    "dissembling",
    "droning",
    "errant",
    "fawning",
    "fobbing",
    "frothy",
    "froward",
    "gleeking",
    "goatish",
    "gorbellied",
    "impertinent",
    "infectious",
    "jarring",
    "loggerheaded",
    "lumpish",
    "mammering",
    "mangled",
    "mewling",
    "paunchy",
    "pribbling",
    "puking",
    "puny",
    "qualling",
    "rank",
    "reeky",
    "roguish",
    "ruttish",
    "saucy",
    "spleeny",
    "spongy",
    "surly",
    "tottering",
    "unmuzzled",
    "vain",
    "venomed",
    "villainous",
    "warped",
    "wayward",
    "weedy",
    "yeasty",
    "abominable",
    "accursed",
    "adulterate",
    "arrogant",
    "babbling",
    "barbarous",
    "base",
    "mumbling",
    "overwheening",
    "perfidious",
    "pestilent",
    "poisonous",
    "pragging",
    "rancorous",
    "rascally",
    "sanctimonious",
    "shameless",
    "slanderous",
    "soulless",
    "spongey",
    "crusty",
    "withered",
    "loathed",
    "tongueless",
    "traitorous",
    "unwholesome",
    "viperous",
    "greasy",
    "obscene",
    "beggarly",
    "scandalous",
    "creeping",
    "lascivious",
    "degenerate",
    "meddling"
];

my $secondPhrase = [
"base-court",
    "prick-eared",
    "puisny-tilted",
    "puke-stockinged",
    "open-arsed",
    "bat-fowling",
    "beef-witted",
    "beetle-headed",
    "boil-brained",
    "clapper-clawed",
    "clay-brained",
    "common-kissing",
    "crook-pated",
    "dismal-dreaming",
    "dizzy-eyed",
    "doghearted",
    "dread-bolted",
    "earth-vexing",
    "elf-skinned",
    "fat-kidneyed",
    "fen-sucked",
    "flap-mouthed",
    "fly-bitten",
    "folly-fallen",
    "fool-born",
    "full-gorged",
    "guts-griping",
    "half-faced",
    "hasty-witted",
    "hedge-born",
    "hell-hated",
    "idle-headed",
    "ill-bred",
    "ill-nurtured",
    "knotty-pated",
    "milk-livered",
    "motley-minded",
    "onion-eyed",
    "plume-plucked",
    "pottle-deep",
    "pox-marked",
    "reeling-ripe",
    "rough-hewn",
    "rude-growing",
    "rump-fed",
    "shard-borne",
    "sheep-biting",
    "spur-galled",
    "swag-bellied",
    "tardy-gaited",
    "tickle-brained",
    "white-livered",
    "toad-spotted",
    "urchin-snouted",
    "weather-bitten",
    "shag-haired",
    "tallow-faced",
    "beef-witted",
    "decayed",
    "deformed",
    "muddy-mottled",
    "hag-born",
    "long-tongued",
    "toad-spotted"
];


my $thirdPhrase = [
"baggage",
    "barnacle",
    "bladder",
    "boar-pig",
    "bugbear",
    "bum-bailey",
    "canker-blossom",
    "clack-dish",
    "clotpole",
    "codpiece",
    "coxcomb",
    "death-token",
    "dewberry",
    "flap-dragon",
    "flax-wench",
    "flirt-gill",
    "foot-licker",
    "fustilarian",
    "giglet",
    "gudgeon",
    "haggard",
    "harpy",
    "hedge-pig",
    "horn-beast",
    "hugger-mugger",
    "joithead",
    "lewdster",
    "lout",
    "maggot-pie",
    "malt-worm",
    "mammet",
    "measle",
    "minnow",
    "miscreant",
    "moldwarp",
    "mumble-news",
    "nut-hook",
    "pigeon-egg",
    "pignut",
    "pumpion",
    "puttock",
    "ratsbane",
    "scut",
    "skainsmate",
    "strumpet",
    "varlet",
    "vassal",
    "wagtail",
    "whey-face",
    "scullion",
    "serpents-egg",
    "callet",
    "slug",
    "bag of guts",
    "punk",
    "bitch-wolf",
    "botch",
    "withered-hag",
    "mangy-dog",
    "foul deformity",
    "odiferous stench",
    "no bowels",
    "drunkard",
    "turd",
    "bear-whelp",
    "eunuch",
    "devil-incarnate",
    "filthy rogue",
    "vile worm",
    "writhled shrimp",
    "scurvy-knave",
    "whore-master",
    "malt-horse",
    "varlet",
    "worms-meat",
    "canker-blossom",
    "carrion",
    "hag-seed",
    "ruinous-butt",
    "contriver",
    "hypocrite",
    "infection",
    "imbossed carbunkle",
    "eternal devil",
    "execrable-wretch",
    "murderous coward",
    "foul adulterer",
    "ingested-lump",
    "wrinkled-witch",
    "plebian",
    "strumpet",
    "horse-drench",
    "promise-breaker",
    "incontinent varlet",
    "leprous witch",
    "babbling gossip",
    "tyrant",
    "purified-cur",
    "misbegotten-divel",
    "mildewed-ear"
];

my $insultsUsed = {};
my $responsesUsed = {};

my $insults = [
	qq^Hi %nick%, you\'re a real nice %insult%.^,
	qq^Congratulations %nick%, quite the %insult% today huh?^,
	qq^%nick%, you\'re a god damn %insult%.^,
	qq^Yo %nick%, your shirt looks like a dishrag, and additionally i think you\'re a %insult%.^,
	qq^Hey %nick%, its about time you were told hwat.. you %insult%.^,
	qq^Dear %nick%, i think you\'re a %insult%.^,
	qq^%nick%: your ass looks like a bag of rigotta cheese you %insult%^,
	qq^What the fuck is this mickey mouse shit %nick%? I mean seriously you %insult%.^,
	qq^Howdy %nick%, i heard that you\'re a %insult%, LOL!^,
	qq^%nick%: it goes without saying that your're a %insult%.\r\n I want to see some god damn improvements next year.^,
	qq^Hello there %nick%, you earned the %insult% award. TA-DA!^,
	qq^Greetings of the day %nick%, ya fuckin %insult%.^,
	qq^Heh %nick%, not much to say about that %insult%. Also this is a private channel, GTFO.^,
	qq^Cheerio \"%nick%\" you classless %insult%.^,
	qq^%nick%: You snowblowin\' %insult%.^,
	qq^Hey everyone, %nick% touched themselves last night like a real %insult%^,

];


my $responses = [

	qq^ %nick%: "You're not good at one thing. Kill yourself." ~ Mark D. Winkelman ^,

	qq^ %nick%, Your client's a scumbag, you're a scumbag, and scumbags see the judge on 
	Monday morning. Now get out of my channel, and take laughing boy with you!
	^,

	qq^ Oooh. Guns, guns, guns. C'mon, %nick%. The Tigers are playing... 
	tonight. I never miss a game.
	^,

	qq^ I don't know. I don't know. Maybe I'm just not making myself clear.  
	I don't want to fuck with you, nick. But I got the connections. I got 
	the sales organization. I got the muscle to shove enough of this office 
	so far up your stupid wop ass that you'll shit AS400's for a year! 
	^,

	qq^
	One of these days %nick%, im going to blow your cocksucking head off.
	^,

	qq^
	Drugs. Gambling. Prostitution. Virgin territory for the man who knows how to run it all.
	Well, I guess we're going to be friends after all, \%nick\%.
	^,

	qq^
	Well listen chief... You built the fucking thing! Now I gotta deal with 
	it? I don't have time for this bullshit!
	^,

	qq^
	%nick%: Don't you get it, you cocksucker! I work for Jeff Pierce! Jeff fucking Pierce! He's the 
	top developer at S24. S24 runs the cops!
	^,

	qq^
	\%nick\%: Congratulations. I remember when I was a young 
	executive for this company. I used to call the old man funny names. 
	"Iron Butt." "Boner." Once I even called him..."asshole." 
	But there was always respect. I always knew where the line was drawn. 
	And you just stepped over it, buddy-boy!
	^,

	qq^
	\%nick\%: You've insulted me. And you've insulted this company with that bastard 
	creation of yours. I had a guaranteed sale with Bizagi. 
	The Innovation program. Integration projects for at least 25 years. 
	Who cares if it worked or not!?
	^,

	qq^
	Can you fly, \%nick\%?
	^,

	qq^
	Honestly \%nick\%, Did you think you think you were an ordinary developer? You're our 
	product. And we can't very well have our products turning against us, can we?
	^,

	qq^
	Heya... \%nick\%. I'm here to see Caleb Mitchell, but after that I got some free time. Maybe you could... fit me in?
	^,

	qq^ 
	Hey \%nick\%, your ass looks like about 150lbs of chewed bubble gum do you know that?
	^,

	qq^
	When I get home, I'm not the boss like I am at work - I slip into a more feminine role. 
	I take everything off and put on my Stella McCartney silk robe. I'll put on red lipstick 
	or red nails, and it lifts my mood. Sexy underwear also gives me a spark.\" - 
	From the memoirs of Jeff Pierceworthy in chapter 10: \"Conquering Manager Mountain.\"
	^,

	qq^
	\%nick\%, you have suffered an emotional shock. I will notify a rape crisis center.
	^,

	qq^
	Caleb Mitchell is wanted for murder. My programming will not allow me to act against an director of this company.
	^,

	qq^
	I guess you're on your knees about now, begging for your life... But life goes on, it's an old story, the fight for love and glory, huh \%nick\%? 
	It's helps if you think of it as a game. Every game has a winner... and a loser. I'm cashing you out, \%nick\%.
	^,

	qq^
	\%nick\% I drive trucks, break arms, and arm wrestle. It's what I love to do, 
	it's what I do best. Being number one is everything. There is no second place. Second sucks.
	^,

	qq^
	You either fight to win or your throw up your hands and admit that you 
	are fucked! (just go for the latter)
	^,

	qq^
	I don't have to take this shit from you. You know who I am? 
	In the field of local-live-home entertainment, I'm a god!
	^,

	qq^
	\%nic\k%, looks to me like the best part of you ran down the crack of your moms ass 
	and ended up as a brown stain on the mattress! I think you've been cheated!
	^,

	qq^
	Oh \%nick\%, im willing to wager if your brains were gun powder there wouldnt be 
	enough to pop a cork out of a fleas tight little ass.
	^

];

my $CHAN = "#uf0";

my $nickList = [];

my $responseHits = {};
my $insultHits = {};

# 1= debug
# 2 = everything else

my $LOGLEVEL = 2;

my $NICKNAME = _newNickName();

my $READY = 0;

#my $insultCount = int( $#{$firstInsults} * $#{$secondInsults} * $#{$thirdInsults} );

#my $sock = new IO::Socket::SSL->new("secure.typotech.net:6697");
my $sock = new IO::Socket::SSL->new("irc.freenode.net:7000");

if ( !$sock ) {
	print qq{Socket does not exist!\n};
	exit(-1);
}

#_putRaw("PASS Checkov");
_putRaw("USER " . _generateUserName() . " mainframe.rootyou.org irc.freenode.net : Richard Drake");
_putRaw("NICK " . $NICKNAME);

#_putRaw("PASS Tupolev");
#_putRaw("USER " . _generateUserName() . " mainframe.rootyou.org mainframe.rootyou.org : Property of Sir C. Ball XXVII");
#_putRaw("NICK " . $NICKNAME);


#-------------------------------------------------------------------------------
#  MESSAGE LOOP (maintains connection and recieves incoming data for parsing)
#-------------------------------------------------------------------------------
while (<$sock>) {

	# PING
	if ( $_ =~ /PING :([^\r]+)/ ) {
		_putRaw("PONG $1");
	}
	else {
		if ( parsemessage($_) eq "-1" ) {
			last;
		}
	}
}

if($sock) {
	$sock = qq{};
}

#-------------------------------------------------------------------------------
#  PARSE OUT ALL DATA WE CARE ABOUT AND DO SOMETHING WITH IT
#-------------------------------------------------------------------------------
sub parsemessage {
	my $message = shift;
	$message =~ s/[:\r\n\002]+//g;
	_trim(\$message);
	my ( $who, $what, $where, $msg ) = split( / /, $message, 4 );
	_trim(\$msg);
	my ( $nick, $host ) = split( /!/, $who );

	#---------------------------------------------------------------------------
	# SHIT TALKING RESPONSE SECTION
	#---------------------------------------------------------------------------


	#-------------------------------------------------------------------------------
	# PRIVMSG EVENTS
	#-------------------------------------------------------------------------------
	if ( $what eq "PRIVMSG") {

	#-----------------------------------------------------------------------
	# !insult TRIGGER DETECTION
	#-----------------------------------------------------------------------
		if ( $msg =~ /^!insult$/ ) {

			my $insultTemplate = _randomInsult();

			_trim(\$insultTemplate);

			my $text = _parseInsult( _randomNick(), $insultTemplate );
			my $personal = "go away.";

			#-------------------------------------------------------------------
			#  ACCOUNT FOR PM's vs Public
			#-------------------------------------------------------------------
			if($where =~/^#/) {
				_putMsg($where,$text);
			} else {
				_putMsg($nick,$personal);		
			}
		}
		elsif ( $msg =~ /^!qt$/ ) {

			my $responseTemplate = _randomResponse();

			_trim(\$responseTemplate);

			my $text = _parseInsult( $nick, $responseTemplate );
			my $personal = "go away.";

			#-------------------------------------------------------------------
			#  ACCOUNT FOR PM's vs Public
			#-------------------------------------------------------------------
			if($where =~/^#/) {
				_putMsg($where,$text);
			} else {
				_putMsg($nick,$personal);		
			}
		}
		#-----------------------------------------------------------------------
		#  
		#-----------------------------------------------------------------------
		elsif ( $msg =~ /^insult (.+)$/i && $where !~ /^#/ ) {
			if(_validateNick($1)) {
				_putMsg( $CHAN, _parseInsult( $1, _randomInsult()));
			} else {
				_putMsg( $nick, "i dont see " . $1 . ".")
			}
		}
		elsif ( $msg =~ /^quote (.+)$/i && $where !~ /^#/) {
			if(_validateNick($1)) {
				_putMsg( "$CHAN", _parseResponse( $1, _randomResponse() ) );
			} else {
				_putMsg( $nick, "i dont see " . $1 . ".")
			}
		}
		elsif ( $msg =~ /^die alphafish$/i && $where !~ /^#/) {
			_shutDown();
		}
		elsif ( $msg =~ /^randnick$/i && $where !~ /^#/) {
			_putRaw("NICK :" . _newNickName());
		}
		elsif ( $msg =~ /^nick ([^\s]+) alphafish$/i && $where !~ /^#/) {
			_putRaw("NICK :" . $1);
		}
		elsif ($msg =~ /^join ([^\s]+) alphafish$/i && $where !~ /^#/) {
			_putRaw("JOIN :" . $1);
		}
		elsif ($msg =~ /^part ([^\s]+) alphafish$/i && $where !~ /^#/) {
			_putRaw("PART :" . $1);
		}
		elsif ($msg) {
			_consoleLog("info",qq{\[$nick\@$where\]: $msg});
		}
	}
	#-------------------------------------------------------------------------------
	# SERVER CODE EVENTS
	#-------------------------------------------------------------------------------
	elsif ( $what =~ /([0-9]{3})/ ) {

	#-------------------------------------------------------------------------------
	# 376: END OF MOTD (this is a good time to join the channel)
	#-------------------------------------------------------------------------------
	if ( $1 eq "376" ) {
		_putRaw("JOIN $CHAN");
	}

	#-------------------------------------------------------------------------------
	# 366: JOINED THE CHANNEL (good time to announce our presence)
	#-------------------------------------------------------------------------------
	elsif ( $1 eq "366" ) {    # joined the channel
			$READY = 1;
	}

	#-------------------------------------------------------------------------------
	# 353: RECEIVED NICK LIST (parse nicks into an array and manage accordingly)
	#-------------------------------------------------------------------------------
	elsif ( $1 eq "353" ) {
		if($LOGLEVEL == 1) {
			_consoleLog("debug",qq{$msg});
		}
		my @names = split( / /, $msg );
		##
		## NORMALIZE NICKS
		##	
		my $element = 0;
		foreach (@names) {
			$_ =~ s/^[\@\%\~\+]//;
			$_ =~ s/$CHAN//;
		    $_ =~ s/$NICKNAME//;
		    $_ =~ s/\@{0,1}Number6//;
			if($_ eq "") {
				_deleteElement(\@names,$element);
			}
			$element++;
		}
		$nickList = \@names;
		_consoleLog("info", "nick list: " . join( " ", @{$nickList} ) );
	}

	#-------------------------------------------------------------------------------
	#  DISPLAY MOTD
	#-------------------------------------------------------------------------------
	elsif ( $1 eq "372" ) {
		_consoleLog("info",qq{$msg});
	}

	#-------------------------------------------------------------------------------
	#  PARSE OUT CHANNEL TOPIC
	#-------------------------------------------------------------------------------
	elsif ( $1 eq "332" ) {
		my $topic = substr( $msg, length($CHAN), length($msg) );
		_trim(\$topic);
		_consoleLog("info",qq{[TOPIC] $topic});
	}

	#-------------------------------------------------------------------------------
	#  PARSE OUT CHANNEL TOPIC AUTHOR w/ TIMESTAMP
	#-------------------------------------------------------------------------------
	elsif ( $1 eq "333" ) {
		my ( $c, $n, $t ) = split( / /, $msg );
		my ( $sec, $min, $hour, $mday, $mon, $year, $wday, $yday, $isdst )
		= localtime($t);
		_consoleLog("info",qq{Set by .:: $n ::. at }
			. ( 1900 + $year ) . qq{-}
			. $mday . qq{-}
			. $mon
			. qq{ at $hour:$min:$sec GMT});
	}

	#-------------------------------------------------------------------------------
	#  NICKNAME IN USE / CHANGE IT
	#-------------------------------------------------------------------------------
	elsif ( $1 eq "433" || $1 eq "432" ) {

		$NICKNAME = _newNickName();

		_consoleLog("info","Can't use " . $NICKNAME);
		_putRaw(qq{"NICK $NICKNAME})
	}
	#-------------------------------------------------------------------------------
	#  UNIMPLEMENTED CODES
	#-------------------------------------------------------------------------------
	else { 
		_consoleLog("debug",qq{$what -> $msg});
	}
}

	#-------------------------------------------------------------------------------
	# ALL OTHER EVENTS
	#-------------------------------------------------------------------------------
	if ( $what eq "MODE" ) {
		_consoleLog("iNFO", qq{$nick changed mode $msg for $where});
	}
	elsif ( $what eq "NOTICE" ) {
		_consoleLog("info", $msg);
	}

	#-------------------------------------------------------------------------------
	# MAINTAIN LIST OF NICKS IN CHANNEL
	#-------------------------------------------------------------------------------
	elsif ( $what eq "JOIN" ) {
		push( @{$nickList}, $nick );
		_consoleLog("inFO",qq{$nick has joined the channel});
	}

	#-------------------------------------------------------------------------------
	# NICK CHANGE
	#-------------------------------------------------------------------------------
	elsif ( $what eq "NICK" ) {
		my $idx = 0;
		foreach my $item (@$nickList) {
			if ( $item eq $nick ) {
				_deleteElement( $nickList, $idx );
				push( @$nickList, $where );
			}
			$idx++;
		}
		_consoleLog("info", qq{$nick changed to $where});
	}

	#-------------------------------------------------------------------------------
	# USER PARTS WAYS WITH THE CHANNEL
	#-------------------------------------------------------------------------------
	elsif ( $what eq "PART" ) {
		my $idx = 0;
		foreach my $item (@$nickList) {
			if ( $item eq $nick ) {
				print qq{[bot] $nick left, removing\n };
				_deleteElement( $nickList, $idx );
			}
			$idx++;
		}
		_consoleLog("iNFO" , qq{$nick has left $where});
	}
	#-------------------------------------------------------------------------------
	# GETTING KICKED
	#-------------------------------------------------------------------------------
	elsif ( $what eq "KICK" ) {
		if($nick =~ /$msg/) {
			_putRaw("JOIN :" . $where);
		}
		_consoleLog("iNFO" , qq{$nick has been kicked from $where});
	}
	#-------------------------------------------------------------------------------
	# USER DISCONNECTS FROM SERVER
	#-------------------------------------------------------------------------------
	elsif ( $what eq "QUIT" ) {
		my $idx = 0;
		foreach my $item (@$nickList) {
			if ( $item eq $nick ) {
				_deleteElement( $nickList, $idx );
			}
			$idx++;
		}
		_consoleLog("iNFO", qq{$nick disconnected ($msg)});
	}
	#-------------------------------------------------------------------------------
	#  ANY COMMANDS NOT IMPLEMENTED WILL LOG HERE
	#-------------------------------------------------------------------------------
	else {
		_consoleLog("debug", $message);
		_consoleLog("debug", qq{WHO: $who / WHAT: $what / WHERE: $where / NICK: $nick / HOST: $host});
	}
}


#-------------------------------------------------------------------------------
#  WRAPPER FOR _putRaw for addressing a channel or nick
#-------------------------------------------------------------------------------
sub _putMsg {
	##
	## CAN BE A NICK OR CHANNEL
	##
	my $destination = shift;
	my $message = shift;

	if( !$sock ) {
		print qq{[error] socket doesnt exist!};
		print qq{Shutting down...}; 
		exit(-1);
	}

	_putRaw(qq{PRIVMSG $destination :$message});
	_consoleLog("info",qq{ $destination -> $message });
}

#-------------------------------------------------------------------------------
#  SEND RAW DATA TO SERVER, USEFUL FOR SENDING COMMANDS AND NOT MESSAGES
#-------------------------------------------------------------------------------
sub _putRaw {
	my $data = shift;
	
	if( !$sock ) {
		print qq{[error] socket doesnt exist!};
		print qq{Shutting down...}; 
		exit(-1);

	}
	#_consoleLog("info","RAWOUT: " . $data);
	print $sock qq{$data\r\n};
}

sub _trim {
	my $text = shift;
	$$text =~ s/^\s{1,}//g;
	$$text =~ s/\s{1,}$//g;
	$$text =~ s/\r\n//g;
	$$text =~ s/\n//g;
	$$text =~ s/\r//g;
	$$text =~ s/\t//g;
}

sub _commify {
	my $ref = shift;
	$ref = reverse $ref;
	$ref =~ s/(\d\d\d)(?=\d)(?!\d*\.)/$1,/g;
	$ref = scalar reverse $ref;
}

sub _randomNick {
	_arrShuffle($nickList);
	return $nickList->[ int( rand( $#{$nickList} ) ) ];
}

sub _newNickName {
	my $names = {
		"Creeping" => [
    	"plebian",
    	"strumpet",
    	"tyrant",
    	"baggage",
    	"barnacle",
    	"bladder"
		],
		"Muffy" => [
		"Diver"
		],
		"Artless" => [
    	"clotpole",
    	"codpiece",
    	"coxcomb",
    	"dewberry",
		"hack"
		],
		"Barbarous" => [
		"nut hook",
    	"pigeon egg",
    	"pignut",
    	"pumpion",
    	"puttock",
    	"ratsbane",
		"bladder"
		],
		"Obscene" => [
		"joithead",
    	"lewdster",
    	"lout",
    	"maggot pie",
    	"malt worm",
    	"mammet",
    	"measle",
    	"minnow",
    	"miscreant",
    	"moldwarp"
		],
		"Clarence" => [
		"J Boddicker"
		]
	};

	my @firstNames = keys %$names;

	_arrShuffle(\@firstNames);

	my $firstName = @firstNames[ int rand( $#firstNames ) ];

	_arrShuffle(scalar $names->{$firstName});

	my $nickname = $firstName . " " . $names->{$firstName}->[ int rand( $#{$names->{$firstName}} ) ];
	_replaceSpaces(\$nickname);
	_consoleLog("info",qq{Nickname generated: $nickname});
	return lc($nickname);
}

sub  _replaceSpaces {
	my $ref = shift;	
	$$ref =~ s/ /_/g
}

sub _randomInsult {

	my $insult = qq{};
	my $words = [];

	_arrShuffle($firstPhrase);
	_arrShuffle($secondPhrase);
	_arrShuffle($thirdPhrase);

	my $idx1 = rand( $#{$firstPhrase} );
	my $idx2 = rand( $#{$secondPhrase} );
	my $idx3 = rand( $#{$thirdPhrase} );

	my $retVal = 	lc( $firstPhrase->[ $idx1 ] ) . " " .
					lc( $secondPhrase->[ $idx2 ] ) . " " .
					lc( $thirdPhrase->[$idx3] );

	return $retVal;
}

sub _randomResponse {

	_arrShuffle( $responses );
	my $numResponses = $#{$responses};
	my $response = $responses->[ rand( $#{$responses} ) ];
	my $idx = 0;

	while(_responseUsed($response)) {
		$idx++;
		$response = $responses->[ rand( $#{$responses} ) ];

		if($idx == $#{$responses}) {
			$responseHits = {};
		}
	}
	return $response;
}

sub _responseUsed {
	my $response = shift;

	if(exists($responseHits->{$response})) {
		return 1;
	} else {
		$responseHits->{$response}++;
		return 0;
	}
}

sub _shutDown {
	print "Die request received\n";
	_putRaw("QUIT :Going down for maintenance. Sorry for the inconvience!");
	sleep(5);
	close($sock);
}

sub _deleteElement {
	my ( $arrRef, $element ) = @_;
	my @temp = splice( @{$arrRef}, $element, 1 );
	$arrRef = \@temp;
}

#-------------------------------------------------------------------------------
# CONSTRUCTS THE FINAL INSULT SENTANCE
#-------------------------------------------------------------------------------
sub _parseInsult {
	my ( $nick, $insult ) = @_;
	$insult =~ s/%nick%/$nick/g;
	$insult =~ s/%insult%/$insult/g;
	return $insult;

}

sub _parseResponse {
	my ( $nick, $text ) = @_;
	$text =~ s/%nick%/$nick/g;
	return $text;
}

#-------------------------------------------------------------------------------
# SWAPS ARRAY VALUES AROUND RANDOMLY
#-------------------------------------------------------------------------------
sub _arrShuffle {
	my $arrRef = shift;
	my $idx    = @$arrRef;
	while ( --$idx ) {
		my $jdx = int rand( $idx + 1 );
		@$arrRef[ $idx, $jdx ] = @$arrRef[ $jdx, $idx ];
	}
}

#-------------------------------------------------------------------------------
# GENERATES RANDOM REAL NAMES (can also work against bans ie *!LOL@*))
#-------------------------------------------------------------------------------
sub _generateUserName {
	my @chars = qw/1 2 3 4 5 6 7 8 9 0 a b c d e f/;
	
	_arrShuffle(\@chars);
	_arrShuffle(\@chars);

	_consoleLog("info" . join(@chars));

	my @string = splice(@chars,0,8);

	_consoleLog("info", "Setting real name to: " . join("",@string));

	return join("",@string);
}

sub _consoleLog {
	my ($type, $text) = @_;

	$type = uc($type);
	
	if(($type =~ /info/i || $type =~ /insult/i) && $LOGLEVEL == 2 || $LOGLEVEL == 1) {
		my 	$data = "[" . $type . "] $text\n ";
		print $data;
	}
	if($LOGLEVEL == 1)	{
		my 	$data = "[" . $type . "] $text\n ";
		print $data;
	}
} 

sub _validateNick {
	my $nick = shift;
	foreach(@$nickList) {
		if($_ eq $nick) {
			return 1;	
		}
	}
	return 0;
}
